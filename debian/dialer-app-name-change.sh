#!/bin/sh

for base in "${XDG_CONFIG_HOME:-${HOME}/.config}" \
    "${XDG_CACHE_HOME:-${HOME}/.cache}"; do
    mv -T "${base}/com.ubuntu.dialer-app" \
        "${base}/dialer-app.ubports" 2>/dev/null
done

exit 0
